﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly EmailService _emailService;

        public PostsController(PostService postService, LikeService likeService, EmailService emailService)
        {
            _postService = postService;
            _likeService = likeService;
            _emailService = emailService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPosts());
        }

        [HttpGet("post")]
        [AllowAnonymous]
        public async Task<ActionResult<PostDTO>> GetPostById([FromQuery] int id)
        {
            return Ok(await _postService.GetPostById(id));
        }

        // Gets portion (page) of posts for Infinity Scrolling.
        // Each page has perPage posts on it 
        [HttpGet("pagination")]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> GetPortionOfPosts([FromQuery] int page, [FromQuery] int perPage)
        {
            return Ok(await _postService.GetPortionOfPosts(page, perPage));
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPut]
        public async Task<ActionResult<PostDTO>> EditPost([FromBody] PostDTO dto)
        {
            dto.Author.Id = this.GetUserIdFromToken();

            return Ok(await _postService.EditPost(dto));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<int>> DeletePost(int id)
        {
            return Ok(await _postService.SoftDeletePost(id));
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikePost(reaction);
            return Ok();
        }

        [HttpPost("email")]
        public async Task<IActionResult> SendPostByEmail([FromBody] SharedPostDTO sharedPost)
        {
            await _emailService.SendSharedPostByEmail(sharedPost);
            return Ok();
        }
    }
}