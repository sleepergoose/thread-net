﻿using System;

namespace Thread_.NET.BLL.Exceptions
{
    public class InvalidEmailAdressException : Exception
    {
        public InvalidEmailAdressException() : base("Invalid email address.") { }
    }
}
