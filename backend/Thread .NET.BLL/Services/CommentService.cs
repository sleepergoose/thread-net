﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using System.Linq;
using Thread_.NET.BLL.Hubs;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        public CommentService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper) 
        {
            _postHub = postHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(post => post.Post)
                    .ThenInclude(a => a.Author)
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            // Sends the created comment with SignalR
            await _postHub.Clients.All.SendAsync("NewComment", _mapper.Map<CommentDTO>(createdComment), createdComment.PostId);

            // Notifies post owner about new comment (with SignalR)
            List< string> userId;
            if(PostHub.UsersIds.TryGetValue(createdComment.Post.Author.UserName, out userId) == true)
                await _postHub.Clients.Clients(userId).SendAsync("Notification", $"User '{createdComment.Author.UserName}' added comment to your post right now!");

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task<CommentDTO> UpdateComment(CommentDTO commentDto)
        {
            var comment = _context.Comments.Where(c => c.Id == commentDto.Id).AsNoTracking().FirstOrDefault();

            if(comment != null)
            {
                comment.Body = commentDto.Body;
                comment.UpdatedAt = System.DateTime.Now;
                _context.Entry(comment).State = EntityState.Modified;
                //_context.Comments.Update(comment);
                await _context.SaveChangesAsync();
            }

            var updatedComment = await _context.Comments
                .Include(post => post.Post)
                    .ThenInclude(a => a.Author)
                .Include(com => com.Author)
                    .ThenInclude(user => user.Avatar)
                .Include(com => com.Reactions)
                .FirstAsync(com => com.Id == commentDto.Id);

            // Sends the created comment with SignalR
            await _postHub.Clients.All.SendAsync("UpdateComment", _mapper.Map<CommentDTO>(updatedComment), updatedComment.PostId);

            // Notifies post owner about new comment (with SignalR)
            List<string> userId;
            if (PostHub.UsersIds.TryGetValue(updatedComment.Post.Author.UserName, out userId) == true)
                await _postHub.Clients.Clients(userId).SendAsync("Notification", $"User '{updatedComment.Author.UserName}' updated his/her comment to your post right now!");


            return _mapper.Map<CommentDTO>(updatedComment);
        }

        
        public async Task<int> SoftDeleteComment(int id)
        {
            DAL.Entities.Comment comment = _context.Comments.FirstOrDefault(c => c.Id == id);

            if (comment != null)
            {
                comment.IsDeleted = true;
                _context.Entry(comment).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }

            // since the soft delete is used
            var deletedComment = await _context.Comments
                .Include(post => post.Post)
                    .ThenInclude(a => a.Author)
                .Include(com => com.Author)
                    .ThenInclude(user => user.Avatar)
                .Include(com => com.Reactions)
                .FirstAsync(com => com.Id == id);

            // Sends the created comment with SignalR
            await _postHub.Clients.All.SendAsync("DeleteComment", _mapper.Map<CommentDTO>(deletedComment), deletedComment.PostId);

            // Notifies post owner about new comment (with SignalR)
            List<string> userId;
            if (PostHub.UsersIds.TryGetValue(deletedComment.Post.Author.UserName, out userId) == true)
                await _postHub.Clients.Clients(userId).SendAsync("Notification", $"User '{deletedComment.Author.UserName}' deleted his/her comment to your post right now!");

            return id;
        }

    }
}
