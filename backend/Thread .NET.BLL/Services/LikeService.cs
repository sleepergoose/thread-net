﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private readonly EmailService _emailService;
        public LikeService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub,
            EmailService emailService) : base(context, mapper)
        {
            _postHub = postHub;
            _emailService = emailService;
        }


        /// <summary>
        /// Sets like to comment
        /// </summary>
        public async Task LikeComment(NewReactionDTO reaction)
        {
            // Gets specified user's likes
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId).ToList();

            /* if the user already has reaction for current comment then it will be removed or changed */
            // remove
            if (likes.Any() && likes.FirstOrDefault().IsLike == reaction.IsLike)
            {
                _context.CommentReactions.RemoveRange(likes);
            }
            // change
            else if (likes.Any() && likes.FirstOrDefault().IsLike != reaction.IsLike)
            {
                _context.CommentReactions.RemoveRange(likes);
                _context.CommentReactions.Add(new DAL.Entities.CommentReaction
                {
                    CommentId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });

            }
            /* otherwise (if the user doesn't have any reaction yet) new reaction will be added */
            else
            {
                _context.CommentReactions.Add(new DAL.Entities.CommentReaction
                {
                    CommentId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
            }

            await _context.SaveChangesAsync();

            // Gets the rated comments and user who rated it
            var post = await _context.Comments
                .Include(comments => comments.Author)
                .FirstAsync(comments => comments.Id == reaction.EntityId);

            var user = await _context.Users
                .FirstAsync(user => user.Id == reaction.UserId);

            // Notifies post owner about new comment (with SignalR)
            List<string> userId;
            if (PostHub.UsersIds.TryGetValue(post.Author.UserName, out userId) == true)
            {
                await _postHub.Clients.Clients(userId).SendAsync("Notification", $"User '{user.UserName}' rated your comment under the post from {post.CreatedAt.ToShortDateString()}");
            }
        }

        /// <summary>
        /// Sets like to post
        /// </summary>
        public async Task LikePost(NewReactionDTO reaction)
        {
            // Gets specified user's likes
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId).ToList();

            /* if the user already has reaction for current post then it will be removed or changed */
            // remove
            if (likes.Any() && likes.FirstOrDefault().IsLike == reaction.IsLike)
            {
                _context.PostReactions.RemoveRange(likes);
            }
            // change
            else if (likes.Any() && likes.FirstOrDefault().IsLike != reaction.IsLike)
            {
                _context.PostReactions.RemoveRange(likes);
                _context.PostReactions.Add(new DAL.Entities.PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
            }
            /* otherwise (if the user doesn't have any reaction yet) new reaction will be added */
            else
            {
                _context.PostReactions.Add(new DAL.Entities.PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
            }

            await _context.SaveChangesAsync();

            // Gets the rated post and user who rated it
            var post = await _context.Posts
                .Include(post => post.Author) 
                .Include(post => post.Preview)
                .FirstAsync(post => post.Id == reaction.EntityId);

            var user = await _context.Users
                .FirstAsync(user => user.Id == reaction.UserId);

            // Notifies post owner about new comment (with SignalR)
            List<string> userId;
            if (PostHub.UsersIds.TryGetValue(post.Author.UserName, out userId) == true)
            {
                await _postHub.Clients.Clients(userId).SendAsync("Notification", $"User '{user.UserName}' rated your post");
            }

            // Sends email with notification when post was rated
            await _emailService.SendEmail("none@server.com", post.Author.UserName, 
                "ThreadNet Notification", "Your post was rated right now!");
        }
    }
}
