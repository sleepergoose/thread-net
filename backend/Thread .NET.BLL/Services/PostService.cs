﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        // Gets portion (page) of posts for Infinity Scrolling.
        // Each page has perPage posts on it 
        public async Task<ICollection<PostDTO>> GetPortionOfPosts(int page, int perPage)
        {
            int skip = (page - 1) * perPage;
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments.Where(com => com.IsDeleted == false))
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(r => r.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                        .ThenInclude(a => a.Avatar)
                .Where(p => p.IsDeleted == false)
                .OrderByDescending(post => post.CreatedAt)
                .Skip(skip)
                .Take(perPage)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments.Where(com => com.IsDeleted == false))
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(r => r.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                        .ThenInclude(a => a.Avatar)
                .Where(p => p.IsDeleted == false)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> GetPostById(int postId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments.Where(com => com.IsDeleted == false))
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(r => r.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                        .ThenInclude(a => a.Avatar)
                .Where(p => p.IsDeleted == false && p.Id == postId)
                .FirstOrDefaultAsync();

            return _mapper.Map<PostDTO>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments.Where(com => com.IsDeleted == false))
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(r => r.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                        .ThenInclude(a => a.Avatar)
                .Where(p => p.AuthorId == userId && p.IsDeleted == false) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);

            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> EditPost(PostDTO postDto)
        {
            var post = _context.Posts.FirstOrDefault(p => p.Id == postDto.Id);

            if (post != null)
            {
                post.UpdatedAt = System.DateTime.Now;
                post.AuthorId = postDto.Author.Id;
                post.Preview = new Image() { CreatedAt = post.CreatedAt, UpdatedAt = post.UpdatedAt, URL = postDto.PreviewImage };
                post.Body = postDto.Body;

                _context.Posts.Update(post);
                await _context.SaveChangesAsync();
            }

            var editedPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments.Where(com => com.IsDeleted == false))
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(r => r.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                        .ThenInclude(a => a.Avatar)
                .FirstAsync(p => p.Id == post.Id);

            var editedPostDTO = _mapper.Map<PostDTO>(editedPost);

            await _postHub.Clients.All.SendAsync("UpdatePost", editedPostDTO);

            return editedPostDTO;
        }

        public async Task<int> SoftDeletePost(int id)
        {
            DAL.Entities.Post post = _context.Posts.FirstOrDefault(p => p.Id == id);

            if (post != null)
            {
                post.IsDeleted = true;
                _context.Entry(post).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }

            await _postHub.Clients.All.SendAsync("DeletePost", id);

            return id;
        }
    }
}
