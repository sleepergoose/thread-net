﻿using System;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.DAL.Context;
using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Thread_.NET.BLL.Hubs;
using System.Text.RegularExpressions;
using Thread_.NET.BLL.Exceptions;
using MailKit;
using MimeKit;
using System.Text;
using MimeKit.Utils;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.BLL.Services
{
    public class EmailService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        public EmailService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        private bool AddressCheck(string address)
        {
            Regex regex = new Regex(@"^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$");
            if (regex.IsMatch(address) == false)
                return false;
            return true;
        }

        public async Task SendPostByEmail(string addressTo, string author, string date, string imageUrl, string postBody, string hostUrl)
        {
            if (AddressCheck(addressTo) == false)
                return;

            try
            {
                MimeMessage message = new MimeMessage();

                message.From.Add(new MailboxAddress("ThreadNet", "threadnet@server.com"));
                message.To.Add(new MailboxAddress(author, addressTo));
                message.Subject = "ThreadNet";

                var builder = new BodyBuilder();

                //var image = builder.LinkedResources.Add(imageUrl);
                //image.ContentId = MimeUtils.GenerateMessageId();

                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"<div style=\"height: 30 %; width: 30 %; padding: 0.5em; margin: 0px auto; border: 1px solid #989898;border-radius: 0.5rem;\">");
                sb.AppendLine($"<h3>{author} <span style=\"color: #888888;font-weight: 100;\">{date}</span></h3>");
                sb.AppendLine($"<img style=\"display:block;height:100%;width:100 %;\" src=\"{imageUrl}\" alt=\"Post image\">");
                sb.AppendLine($"<br>");
                sb.AppendLine($"<hr>");
                sb.AppendLine($"<div style=\"text-align:justify;text-indent: 7%;\">");
                sb.AppendLine($"{postBody}");
                sb.AppendLine($"</div>");
                sb.AppendLine($"</div>");
                sb.AppendLine($"<a href=\"{hostUrl}\">Join to ThreadNet</a>");
                
                builder.HtmlBody = sb.ToString();
                message.Body = builder.ToMessageBody();

                using (MailKit.Net.Smtp.SmtpClient client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.Connect("smtp.gmail.com", 465, true);
                    client.Authenticate("threadnet@server.com", "password");
                    await client.SendAsync(message);

                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                // Turned off for debug without sending emails
                // throw new Exception(ex.Message);
            }
        }

        public async Task SendEmail(string addressTo, string receiverName, string subject, string body)
        {
            if (AddressCheck(addressTo) == false)
                throw new InvalidEmailAdressException();

            try
            {
                MimeMessage message = new MimeMessage();

                message.From.Add(new MailboxAddress("ThreadNet", "threadnet@server.com")); 
                message.To.Add(new MailboxAddress(receiverName, addressTo)); 
                message.Subject = subject; 
                message.Body = new BodyBuilder() 
                { 
                    TextBody = body
                }.ToMessageBody(); 

                using (MailKit.Net.Smtp.SmtpClient client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.Connect("smtp.gmail.com", 465, true); 
                    client.Authenticate("threadnet@server.com", "password"); 
                    await client.SendAsync(message);

                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                // Turned off for debug without sending emails
                // throw new Exception(ex.Message);
            }
        }

        // Sends post by email
        public async Task SendSharedPostByEmail(SharedPostDTO sharedPostDTO)
        {
            await SendPostByEmail(
                                  addressTo: sharedPostDTO.Email,
                                  author: sharedPostDTO.Post.Author.UserName,
                                  date: sharedPostDTO.Post.CreatedAt.ToShortDateString(),
                                  imageUrl: sharedPostDTO.Post.PreviewImage,
                                  postBody: sharedPostDTO.Post.Body,
                                  hostUrl: $"http://localhost:4200/post?id={sharedPostDTO.Post.Id}");
        }

    }
}
