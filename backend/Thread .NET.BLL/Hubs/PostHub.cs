﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class PostHub : Hub
    {
        // List of users who connected to the hub
        public static Dictionary<string, List<string>> UsersIds = new Dictionary<string, List<string>>();

        // Fill UsersIds list
        public void RegisterConnection(string userName, string id)
        {
            if (UsersIds.TryGetValue(userName, out _) == false)
                UsersIds.Add(userName, new List<string> { id });
            else
                UsersIds[userName].Add(id);
        }

        public async Task Send(string message)
        {
            await Clients.All.SendAsync("Notification", message);
        }
    }
}
