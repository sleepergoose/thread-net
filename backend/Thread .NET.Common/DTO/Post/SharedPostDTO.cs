﻿namespace Thread_.NET.Common.DTO.Post
{
    public class SharedPostDTO
    {
        public PostDTO Post { get; set; }
        public string Email { get; set; }

    }
}
