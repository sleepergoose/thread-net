﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thread_.NET.Common.DTO.User
{
    public sealed class UserPasswordResetDTO
    {
        public int UserId { get; set; }
        public string Password { get; set; }
    }
}
