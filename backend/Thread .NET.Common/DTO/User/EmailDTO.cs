﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thread_.NET.Common.DTO.User
{
    public class EmailDTO
    {
        public string Email { get; set; }
    }
}
