import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService,
        private commentService: CommentService) { }

    /* ========================== */
    // Sets the like of a comment   
    /* ========================== */
    public likeComment(comment: Comment, currentUser: User, like: boolean | null) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: like,
            userId: currentUser.id
        };

        /* update current array instantly */
        // check if user reaction already exists and save his previous reaction
        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);
        let previousReaction: boolean | null = null;

        // if it exists
        if (hasReaction) {
            let userReaction = innerComment.reactions.filter((x) => x.user.id === currentUser.id)[0];
            previousReaction = userReaction.isLike;
            switch (like === userReaction.isLike) {
                case true:
                    userReaction.isLike = null;
                    break;
                case false:
                    userReaction.isLike = like;
                    break;
            }
        }
        // otherwise (if user reaction doesn't exist yet)
        else {
            innerComment.reactions = innerComment.reactions.concat({ isLike: like, user: currentUser });
        }

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                if (hasReaction) {
                    innerComment.reactions.filter((x) => x.user.id === currentUser.id)[0].isLike = previousReaction;
                } else {
                    innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
                }
                return of(innerComment);
            })
        );
    }

    /* ======================= */
    // Sets the like of a post    
    /* ======================= */
    public likePost(post: Post, currentUser: User, like: boolean | null) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: like,
            userId: currentUser.id
        };

        /* update current array instantly */
        // check if user reaction already exists and save his previous reaction
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        let previousReaction: boolean | null = null;

        // if it exists
        if (hasReaction) {
            let userReaction = innerPost.reactions.filter((x) => x.user.id === currentUser.id)[0];
            previousReaction = userReaction.isLike;
            switch (like === userReaction.isLike) {
                case true:
                    userReaction.isLike = null;
                    break;
                case false:
                    userReaction.isLike = like;
                    break;
            }
        }
        // otherwise (if user reaction doesn't exist yet)
        else {
            innerPost.reactions = innerPost.reactions.concat({ isLike: like, user: currentUser });
        }

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                if (hasReaction) {
                    innerPost.reactions.filter((x) => x.user.id === currentUser.id)[0].isLike = previousReaction;
                } else {
                    innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
                }
                return of(innerPost);
            })
        );
    }
}
