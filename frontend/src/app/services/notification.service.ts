import { Injectable } from '@angular/core';
import * as signalR from "@microsoft/signalr";
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class NotificationService {
	private baseUrl: string = environment.apiUrl;
	private signalrUrl: string = "/notifications/post";
	private hubConnection: signalR.HubConnection;

	constructor() { }

	public createConnection() {
		this.hubConnection = new signalR.HubConnectionBuilder()
			.withUrl(`${this.baseUrl}${this.signalrUrl}`)
			.withAutomaticReconnect()
			.build();
	}

	public startConnection = () => {
		this.hubConnection
			.start()
			.then(() => { console.log('Connection started') })
			.catch(err => console.log('Error while starting connection: ' + err));
	}

    // Stops SignalR connection
	public stopConnection() {
		this.hubConnection.stop();
	}

	// Adds listener for data receiving
	public addNotificationListener = (listenerName: string, listenerCallback: any) => {
		this.hubConnection.on(listenerName, listenerCallback);
	}

	// Sends registered user name and connectionId
	public SendConnectionId(userName: string) {
		this.hubConnection.invoke("RegisterConnection", userName, this.hubConnection.connectionId);
	}
}

