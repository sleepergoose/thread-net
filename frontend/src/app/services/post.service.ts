import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    // Gets portion (page) of posts for Infinity Scrolling.
    public GetPortionOfPosts(page: number, perPage: number) {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}/pagination`, { page, perPage });
    }

    public getPostById(id: number) {
        return this.httpService.getFullRequest<Post>(`${this.routePrefix}/post`, { id });
    }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public editPost(editedPost: Post) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}`, editedPost);
    }

    public deletePost(deletedPostId: any) {
        return this.httpService.deleteFullRequest<number>(`${this.routePrefix}/${deletedPostId}`);
    }
}
