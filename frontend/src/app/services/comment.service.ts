import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { Reaction } from '../models/reactions/reaction';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    // This was given
    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    // Sets the like of a comment
    public likeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Reaction>(`${this.routePrefix}/like`, reaction);
    }

    // Updates a comment
    public updateComment(comment: Comment) {
        return this.httpService.putFullRequest<Comment>(`${this.routePrefix}`, comment);
    }

    // Delete a comment
    public deleteComment(deletedCommentId: any) {
        return this.httpService.deleteFullRequest<number>(`${this.routePrefix}/${deletedCommentId}`);
    }
}
