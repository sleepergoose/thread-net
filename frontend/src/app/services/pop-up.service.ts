import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopUpComponent } from "../components/pop-up/pop-up.component"
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PopUpDialogService {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) { }

    public openPopUpDialog(data: any[], reaction: boolean) {
        const dialog = this.dialog.open(PopUpComponent, {
            data: {
                keyBody: data,
                keyReaction: reaction
            },
            minWidth: 400,
            maxHeight: 600,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });

        dialog
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((result: any) => {
                if (result) {
                    console.log("result: " + result);
                }
            });
    }
}