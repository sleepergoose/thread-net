import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { InputDialogComponent } from '../components/input-dialog/input-dialog.component';
import { Post } from '../models/post/post';
import { HttpInternalService } from './http-internal.service';
import { SnackBarService } from './snack-bar.service';


@Injectable({
	providedIn: 'root'
})
export class EmailService {
	private sharedPost: Post;

	constructor(
		private dialog: MatDialog,
		private httpService: HttpInternalService,
		private snackBarService: SnackBarService) { }

	private openInputDialog(data?: any): any {
		const dialog = this.dialog.open(InputDialogComponent, {
			data: {
				key: data,
			},
			minWidth: 300,
			maxHeight: 300,
			autoFocus: true,
			backdropClass: 'dialog-backdrop',
			position: {
				top: '0'
			}
		});

		dialog.afterClosed().subscribe(email => {
			if (email) {
				this.sendEmailWithSharedPost(email);
			}
		})
	}

	public sharePostByEmail(post: Post) {
		this.sharedPost = post;
		this.openInputDialog();
	}

	private sendEmailWithSharedPost(email: string) {
		let payload = { post: this.sharedPost, email: email };
		this.httpService.postFullRequest<void>("/api/posts/email", payload)
			.subscribe((data) => {
				this.snackBarService.showUsualMessage(`Email to '${email}' was sended successfully!`);
			},
				(error) => console.log("error: " + error))
	}

	public sendEmailToResetPassword(email: string) {
		let payload = { email: email };
		this.httpService.postFullRequest<void>("/api/users/email", payload)
			.subscribe((data) => {
				this.snackBarService.showUsualMessage(`Email to '${email}' was sended successfully!`);
			},
				(error) => this.snackBarService.showErrorMessage(`There is no user with email '${email}`));
	}
}
