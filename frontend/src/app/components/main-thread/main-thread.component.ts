import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { GyazoService } from 'src/app/services/gyazo.service';
import { DatePipe } from '@angular/common';
import { NotificationService } from 'src/app/services/notification.service';
import { Comment } from '../../models/comment/comment';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass'],
    providers: [DatePipe]
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    private unsubscribe$ = new Subject<void>();

    /* ====== Added by me (variables) ====== */
    public isIHaveLiked = false; // flag for posts I've liked
    public isPostEdited = false; // toggle between sendPost button and editPost button on the view
    public isHiddenMine = false; // hide my posts flag
    public editedPost = {} as Post;
    private x: number = 0;
    private y: number = 0;

    // for Infinity Scrolling
    public throttle: number = 0;
    public distance: number = 2;
    private postsPage: number = 1;
    private postsPerPage: number = 4;

    private isStopScrolling: boolean = false;
    /* ==================================== */

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private gyazoService: GyazoService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private datepipe: DatePipe,
        private notificationService: NotificationService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();

        // stops SignalR connection
        this.notificationService.stopConnection();
    }

    public ngOnInit() {
        this.loadingPosts = true;
        // Shows a single post by link
        const parameters = new URLSearchParams(window.location.search);
        let id: number = parseInt(parameters.get('id'));

        if (id) {
            this.isStopScrolling = true;

            this.postService.getPostById(id)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (resp) => {
                        this.loadingPosts = false;
                        this.posts.push(resp.body);
                        this.cachedPosts = this.posts;
                    },
                    (error) => (this.loadingPosts = false)
                );
        } else {
            // Shows all posts
            this.isStopScrolling = false;

            this.postService
                .GetPortionOfPosts(this.postsPage, this.postsPerPage)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (resp) => {
                        this.loadingPosts = false;
                        this.posts = resp.body;
                        this.cachedPosts = resp.body;
                    },
                    (error) => (this.loadingPosts = false)
                );
        }

        this.getUser();

        // created (builds) SignalR connection
        this.notificationService.createConnection();

        // Sets notification listeners (SignalR)
        this.setNotificationListeners();

        // starts (builds) SignalR connection
        this.notificationService.startConnection();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;

            if (this.currentUser) {
                this.notificationService.SendConnectionId(this.currentUser.userName);
            }
        });
    }

    // Infinity Scrolling 
    onScroll(): void {
        if (this.isStopScrolling === false) {
            this.getPage(++this.postsPage, this.postsPerPage);
        }
    }

    /* === COMMENT SECTION === */
    public addNewComment(newComment: Comment, postId: number) {
        const tempPost = this.posts.filter((x) => x.id === postId)[0];
        if (!tempPost.comments.some(c => c.id === newComment.id)) {
            tempPost.comments = this.sortCommentArray(tempPost.comments.concat(newComment));
        } else {
            tempPost.comments = tempPost.comments.filter((p) => p.id !== newComment.id);
            tempPost.comments = this.sortCommentArray(tempPost.comments.concat(newComment));
        }
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }


    /* === POSTS SECTION === */
    // Gets all posts from a server
    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    // Sends a new post
    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.url;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    // Edit post
    public showPostEditor(post: Post) {
        this.isPostEdited = true;
        this.editedPost.id = post.id;
        this.editedPost.createdAt = post.createdAt;
        this.editedPost.author = post.author;
        this.editedPost.previewImage = post.previewImage;
        this.editedPost.comments = null;
        this.editedPost.reactions = null;

        // show data in editor
        this.post.body = post.body;
        this.post.previewImage = post.previewImage;
        this.post.authorId = post.author.id;
        this.imageUrl = post.previewImage;

        this.showPostContainer = true;
        this.x = window.scrollX;
        this.y = window.scrollY;
        window.scrollTo(0, 0);
    }

    // Sends an updated post
    public sendEditedPost() {
        this.editedPost.previewImage = this.imageUrl;
        this.editedPost.body = this.post.body;

        const postSubscription = !this.imageFile
            ? this.postService.editPost(this.editedPost)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.url;
                    return this.postService.editPost(this.editedPost);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
                this.showPostContainer = false;
                this.isPostEdited = false;
                window.scrollTo(this.x, this.y);
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    // Deletes post 
    public deletePost(post: Post) {

        let result: boolean = confirm("Are you really want to delete the post created at " + this.datepipe.transform(post.createdAt, 'd/M/yy, h:mm a'));
        if (result) {
            const postSubscription = this.postService.deletePost(post.id);

            this.loading = true;

            postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    this.posts = this.posts.filter(p => p.id !== resp.body);
                    this.cachedPosts = this.cachedPosts.filter(p => p.id !== resp.body);
                    this.loading = false;
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        }
    }

    // Gets portion posts from a server
    public getPage(page: number, perPage: number) {
        this.loadingPosts = true;
        this.postService
            .GetPortionOfPosts(page, perPage)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.cachedPosts = this.cachedPosts.concat(resp.body);
                    this.postReload();
                },
                (error) => (this.loadingPosts = false)
            );
    }

    // Reoads posts depend on sliders' state
    private postReload() {
        this.loadingPosts = false;

        if (this.isOnlyMine) {
            this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id);
        } else if (this.isHiddenMine) {
            this.posts = this.cachedPosts.filter(x => x.author.id !== this.currentUser.id);
        } else if (this.isIHaveLiked) {
            this.posts = this.cachedPosts.filter(x => x.reactions.filter(r => r.user.id == this.currentUser.id && r.isLike === true).length > 0);
        } else {
            this.posts = this.cachedPosts;
        }
    }

    // 
    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            this.postReload();
        } else {
            this.cachedPosts = this.cachedPosts.filter((p) => p.id !== newPost.id);
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            this.postReload();
        }
    }

    // Sorts posts
    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }


    /* === POST IMAGE SECTION === */
    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }


    /* SLIDERS SECTION */
    public sliderChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isIHaveLiked = false; // uncheck slider 'I've liked'
            this.isHiddenMine = false; // uncheck slider 'Hide mine'
            this.isOnlyMine = true;
            this.posts = this.cachedPosts.filter((x) => x.author.id === this.currentUser.id);
        } else {
            this.isIHaveLiked = false; // uncheck slider 'I've liked'
            this.isOnlyMine = false;
            this.isHiddenMine = false; // uncheck slider 'Hide mine'
            this.posts = this.cachedPosts;
        }
    }

    // Shows posts I have liked
    public sliderIHaveLikedChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyMine = false; // uncheck slider 'Only mine'
            this.isHiddenMine = false; // uncheck slider 'Hide mine'
            this.isIHaveLiked = true;
            this.posts = this.cachedPosts.filter(x => x.reactions.filter(r => r.user.id == this.currentUser.id && r.isLike === true).length > 0);
        } else {
            this.isOnlyMine = false;
            this.isIHaveLiked = false;
            this.isHiddenMine = false;
            this.posts = this.cachedPosts;
        }
    }

    // Hide my posts
    public sliderHideMineChanged(event: MatSlideToggleChange) {
        if (event.checked) {
            this.isOnlyMine = false; // uncheck slider 'Only mine'
            this.isIHaveLiked = false; // uncheck slider 'I've liked'
            this.isHiddenMine = true;
            this.posts = this.cachedPosts.filter(x => x.author.id !== this.currentUser.id);
        } else {
            this.isOnlyMine = false; // uncheck slider 'Only mine'
            this.isIHaveLiked = false;
            this.isHiddenMine = false;
            this.posts = this.cachedPosts;
        }
    }


    /* OTHERS */
    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    // Sets notification listeners (SignalR)
    private setNotificationListeners() {

        // General notification
        this.notificationService.addNotificationListener("Notification", (data) => {
            this.snackBarService.showUsualMessage(data);
        });

        // SignalR listener for new post
        this.notificationService.addNotificationListener('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
                this.snackBarService.showUsualMessage(`User '${newPost.author.userName}' added a new post. Have a look at it!`);
            }
        });

        // SignalR listener when post updated
        this.notificationService.addNotificationListener('UpdatePost', (updatedPost: Post) => {
            if (updatedPost) {
                this.addNewPost(updatedPost);
                this.snackBarService.showUsualMessage(`User '${updatedPost.author.userName}' updated a post. Have a look at it!`);
            }
        });

        // SignalR listener when post deleted
        this.notificationService.addNotificationListener('DeletePost', (id: number) => {
            if (id) {
                const postAuthor = this.posts.filter(p => p.id === id).map(p => p.author.userName)[0];
                this.posts = this.posts.filter(p => p.id !== id);
                this.cachedPosts = this.cachedPosts.filter(p => p.id !== id);
                this.snackBarService.showUsualMessage(`User '${postAuthor}' deleted a post.`);
            }
        });

        /* COMMENTS */
        // SignalR listener for a new comment
        this.notificationService.addNotificationListener('NewComment', (newComment: Comment, postId: number) => {
            if (newComment) {
                this.addNewComment(newComment, postId);
            }
        });

        // SignalR listener when comment updated
        this.notificationService.addNotificationListener('UpdateComment', (updatedComment: Comment, postId: number) => {
            if (updatedComment) {
                this.addNewComment(updatedComment, postId);
            }
        });

        // SignalR listener when comment deleted
        this.notificationService.addNotificationListener('DeleteComment', (deletedComment: Comment, postId: number) => {
            if (deletedComment) {
                const tempPost = this.posts.filter((x) => x.id === postId)[0];
                tempPost.comments = tempPost.comments.filter(c => c.id !== deletedComment.id);
            }
        });
    }
}
