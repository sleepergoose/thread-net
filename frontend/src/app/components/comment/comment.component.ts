import { Component, Input, Output, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { AuthenticationService } from 'src/app/services/auth.service';
import { LikeService } from 'src/app/services/like.service';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';

import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { PopUpDialogService } from '../../services/pop-up.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy, OnInit {

    @Input() public comment: Comment;
    @Input() public currentUser: User;

    @Output() onEditComment = new EventEmitter<Comment>();
    @Output() onDeleteComment = new EventEmitter<Comment>();

    public likes: number = 0;
    public dislikes: number = 0;
    public isCommentEdit: boolean = false;

    private unsubscribe$ = new Subject<void>();

    public ngOnInit() {
        this.likes = this.comment.reactions.filter(x => x.isLike === true).length;
        this.dislikes = this.comment.reactions.filter(x => x.isLike === false).length;
    }

    constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private popUpDialogService: PopUpDialogService) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    // Sets the like of a comment    
    public likeComment(like: boolean | null) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp, like)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.unsubscribe$.subscribe(() =>
            this.likes = this.comment.reactions.filter(x => x.isLike === true).length);
        this.unsubscribe$.subscribe(() =>
            this.dislikes = this.comment.reactions.filter(x => x.isLike === false).length);

        this.likeService
            .likeComment(this.comment, this.currentUser, like)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => {
                this.comment = comment;
                this.unsubscribe$.next();
            });
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public showCommentEditor(comment: Comment) {
        this.isCommentEdit = !this.isCommentEdit;
    }

    public updateComment() {
        this.isCommentEdit = !this.isCommentEdit;
        this.onEditComment.emit(this.comment);
    }

    public deleteComment(comment: Comment) {
        this.onDeleteComment.emit(comment);
    }

    public whoLikedComment(reaction: boolean) {
        const users = this.comment.reactions.filter(r => r.isLike === reaction).map(r => r.user);
        this.popUpDialogService.openPopUpDialog(users, reaction);
    }
}
