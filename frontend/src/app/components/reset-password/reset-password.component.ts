import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../models/user';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { UserService } from '../../services/user.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { NgForm } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
	selector: 'app-reset-password',
	templateUrl: './reset-password.component.html',
	styleUrls: ['./reset-password.component.sass']
})
export class ResetPasswordComponent implements OnInit {
	public user = {} as User;
	public loading = false;
	public hidePass = true;
	public confirmed: boolean = false;

	public newPasswordConfirm: string;
	public newPassword: string;

	private unsubscribe$ = new Subject<void>();

	constructor(
		private location: Location,
		private userService: UserService,
		private snackBarService: SnackBarService,
		private router: Router) { }

	public ngOnInit() {
		const parameters = new URLSearchParams(window.location.search);
		let userId: number = parseInt(parameters.get('id'));

		this.userService
			.getUserByIdMy(userId)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe((response) => {
				this.user = response.body;
			},
				(error) => this.snackBarService.showErrorMessage(error));

	}

	public ngOnDestroy() {
		this.unsubscribe$.next();
		this.unsubscribe$.complete();
	}

	public saveNewPassword(userForm: NgForm) {
		if (userForm.valid && this.confirmed === true) {

			this.loading = true;

			this.userService
				.resetUserPassword({ userId: this.user.id, password: this.newPassword })
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe((response) => {
					this.loading = false;
					this.snackBarService.showUsualMessage("Password has been changed successfully");
					this.confirmed = false;
					this.router.navigate(["thread"]);
					this.newPasswordConfirm = "";
					this.newPassword = "";
				},
					(error) => this.snackBarService.showErrorMessage(error));

		}
	}

	public onChangeConfirm() {
		if (this.newPassword !== this.newPasswordConfirm) {
			this.confirmed = false;
		} else {
			this.confirmed = true;
		}
	}

	public goBack = () => this.location.back();
}



