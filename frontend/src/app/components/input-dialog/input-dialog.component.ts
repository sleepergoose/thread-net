import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-input-dialog',
	templateUrl: './input-dialog.component.html',
	styleUrls: ['./input-dialog.component.css']
})
export class InputDialogComponent implements OnInit {
	public inputData: string;
	error: string;

	constructor(
		private dialogRef: MatDialogRef<InputDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) {
	}

	ngOnInit(): void {
		this.inputData = this.data.key;
	}

	public close() {
		this.dialogRef.close(false);
	}

	public accept(ngForm: NgForm) {
		if (ngForm.valid) {
			this.dialogRef.close(this.inputData);
		}
	}
}
