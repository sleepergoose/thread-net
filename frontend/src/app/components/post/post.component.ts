import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { DatePipe } from '@angular/common';
import { EmailService } from '../../services/email.service';
import { PopUpDialogService } from '../../services/pop-up.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit, OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    @Output() onChanged = new EventEmitter<Post>();
    @Output() onClickAtDelete = new EventEmitter<Post>();

    public showComments = false;
    public newComment = {} as NewComment;

    private unsubscribe$ = new Subject<void>();

    public likes: number = 0;
    public dislikes: number = 0;

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private datepipe: DatePipe,
        private popUpDialogService: PopUpDialogService,
        private emailService: EmailService
    ) { }

    public ngOnInit() {
        this.likes = this.post.reactions.filter(x => x.isLike === true).length;
        this.dislikes = this.post.reactions.filter(x => x.isLike === false).length;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    // Sets the like of a post    
    public likePost(like: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, like)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.unsubscribe$.subscribe(() =>
            this.likes = this.post.reactions.filter(x => x.isLike === true).length);
        this.unsubscribe$.subscribe(() =>
            this.dislikes = this.post.reactions.filter(x => x.isLike === false).length);

        this.likeService
            .likePost(this.post, this.currentUser, like)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;
                this.unsubscribe$.next();
            });
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments.concat(resp.body);
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    public onEditPost(post: Post) {
        this.onChanged.emit(post);
    }

    public onDeletePost(post: Post) {
        this.onClickAtDelete.emit(post);
    }

    public updateComment(comment: Comment) {

        this.commentService
            .updateComment(comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (response) => {
                    if (response) {
                        this.post.comments = this.post.comments.filter(coms => coms.id !== response.body.id);
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(response.body));
                        this.newComment.body = undefined;
                        this.snackBarService.showUsualMessage(`Comment was successfully updated!`);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public deleteComment(comment: Comment) {
        let result = confirm("Are you really want to delete the comment created at " + this.datepipe.transform(comment.createdAt, 'd/M/yy, h:mm a'));
        if (result) {
            const commentSubscription = this.commentService.deleteComment(comment.id);

            commentSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
                (resp) => {
                    this.post.comments = this.post.comments.filter(p => p.id !== resp.body);
                    this.snackBarService.showUsualMessage(`Comment was successfully deleted!`);
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        }
    }

    // Shows pop up with list who likes the post
    public whoLikedPost(reaction: boolean) {
        const users = this.post.reactions.filter(r => r.isLike === reaction).map(r => r.user);
        this.popUpDialogService.openPopUpDialog(users, reaction);
    }

    // Sends post by email
    public sharePost(post: Post) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.emailService.sharePostByEmail(post);
    }

    public copyLink(id: number) {
        this.copy(id);
    }

    private copy(id: number) {
        const input = document.createElement("input");
        input.textContent = `${location.host}/post?id=${id}`;

        try {
            if ((navigator as any).clipboard) {
                (navigator as any).clipboard.writeText(input.textContent);
            } else if ((window as any).clipboardData) { // for Internet Explorer
                (window as any).clipboardData.setData('text', input.textContent);
            } else { // this is for other browsers, iOS, Mac OS
                this.copyToClipboard(input);
            }
            this.snackBarService.showUsualMessage("Link copied to clipboard");
        } catch (e) {
            this.snackBarService.showErrorMessage("Link didn't copy to clipboard");
        }
    }
    private copyToClipboard(el: HTMLInputElement) {
        const oldContentEditable = el.contentEditable;
        const oldReadOnly = el.readOnly;
        try {
            el.contentEditable = 'true'; //  for iOS
            el.readOnly = false;
            this.copyNodeContentsToClipboard(el);
        } finally {
            el.contentEditable = oldContentEditable;
            el.readOnly = oldReadOnly;
        }
    }
    private copyNodeContentsToClipboard(el: HTMLInputElement) {
        const range = document.createRange();
        const selection = window.getSelection();
        range.selectNodeContents(el);
        selection.removeAllRanges();
    }
}
