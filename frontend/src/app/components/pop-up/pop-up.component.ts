import { Component, Input, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/models/user';



@Component({
	selector: 'app-pop-up',
	templateUrl: './pop-up.component.html',
	styleUrls: ['./pop-up.component.css']
})
export class PopUpComponent implements OnInit {
	public users: User[] | undefined;
	public reaction: boolean | undefined;

	constructor(
		private dialogRef: MatDialogRef<PopUpComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) { }

	ngOnInit(): void {
		this.users = this.data.keyBody as User[];
		this.reaction = this.data.keyReaction as boolean;
	}

	public close() {
		this.dialogRef.close();
	}

}
