export interface User {
    id: number;
    email: string;
    userName: string;
    avatar: string;
}

export interface UserResetPasswordDTO {
    userId: number;
    password: string
}
