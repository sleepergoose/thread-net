export interface NewReaction {
    entityId: number;
    isLike: boolean | null;
    userId: number;
}
